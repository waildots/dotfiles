" ============ Settings ============ "

colorscheme gruvbox

set vicmd=nvim
set syscalls
set trash
set history=100
set nofollowlinks
set sortnumbers
set sort=iname
set undolevels=100
set vimhelp
set norunexec
set timefmt=%Y-%m-%d\ %H:%M
set wildmenu
set wildstyle=popup
set suggestoptions=normal,visual,view,otherpane,keys,marks,registers
set incsearch
set ignorecase
set smartcase
"set relativenumber
set sizefmt=units:iec,precision:2,space
set nohlsearch
set scrolloff=4
set dotfiles
set statusline="                            Permissions : %2A %15u:%-15g Size: %-22E Last Modified : %d  "
set vifminfo=dhistory,savedirs,chistory,state,tui,shistory,
    \phistory,fhistory,dirstack,registers,bookmarks,bmarks

" ============ Marks ============ "

mark h ~/
mark d ~/dl
mark p ~/pc
mark s ~/pc/S4
mark H /mnt/hdd
mark a /mnt/phone
mark c ~/.config
mark b ~/.local/bin

" ============ Commands ============ "

command! df df -h %m 2> /dev/null
command! diff vim -d %f %F
command! zip zip -r %f.zip %f
command! run !! ./%f
command! make !!make %a
command! mkcd :mkdir %a | cd %a
command! vgrep vim "+grep %a"
command! reload :write | restart full
command! gal sxiv -t %f %i 
command! dragon dragontop %f %i
command! dragonall dragontop -a -x %f %i
command! extract ext %f


" ============ Normal mode ============ "

nnoremap M :marks<cr>
nnoremap B :bmarks<cr>

nnoremap cx :!chmod +x %f<cr>
nnoremap md :mkcd<space>
nnoremap mk :mkdir<space>
nnoremap mf :!touch<space>
nnoremap rr :file<space><tab>
nnoremap gb :file &<cr>l
nnoremap yd :!echo -n %d | xclip -selection clipboard %i<cr>:echo expand('%"d') "is yanked to clipboard"<cr>
nnoremap yf :!echo -n %c:p | xclip -selection clipboard %i<cr>:echo expand('%"c:p') "is yanked to clipboard"<cr>
nnoremap yn :!echo -n %c | xclip -selection clipboard %i<cr>:echo expand('%"c') "is yanked to clipboard"<cr>

nnoremap ,a :!
nnoremap ,e :tabnew<cr>
nnoremap ,s :sync<cr>
nnoremap ,o :sync %c<cr>
nnoremap ,x <c-w>x
nnoremap ,p :!sxiv -t %d & <cr>
nnoremap ,m :file<cr>
nnoremap ,h :history<cr>
nnoremap ,t :!alacritty &<cr>
nnoremap ,n :!alacritty -e nvim %f &<cr>
nnoremap ,r :!bash %f<cr>
nnoremap ,y :!bash %f<space>
nnoremap ,c :write | edit $MYVIFMRC | restart full<cr>
nnoremap ,w :set wrap!<cr>
nnoremap ,z :extract <cr>
nnoremap <silent> ,b :set viewcolumns=*{name}..,9{}.<cr>
nnoremap <silent> ,d :set viewcolumns=*{name}.,10{perms},12{uname},-7{gname},10{size}.,20{mtime}<cr>

nnoremap f /^
nnoremap o :!xdg-open %f %i<cr>
nnoremap w :view<cr>

nnoremap = :only<cr>
nnoremap | :vsplit<cr>
nnoremap - <C-w>4<
nnoremap _ <C-w>4>

nnoremap I cw<c-a>
nnoremap cc cw<c-u>
nnoremap A cw

nnoremap <f2> :rename<cr>
nnoremap <f4> :!$TERMINAL &<cr>

nnoremap J 5j
nnoremap K 5k

nmap <C-l> :cd<space>
nmap <C-s> :dragon<CR>
nmap <C-b> :dragonall<CR>

nnoremap S :sort<cr>
nnoremap se :set sort=+ext<cr>
nnoremap sE :set sort=-ext<cr>
nnoremap sn :set sort=+iname<cr>
nnoremap sN :set sort=-iname<cr>
nnoremap st :set sort=+type<cr>
nnoremap sT :set sort=-type<cr>
nnoremap ss :set sort=+size<cr>
nnoremap sS :set sort=-size<cr>
nnoremap sa :set sort=+atime<cr>
nnoremap sA :set sort=-atime<cr>
nnoremap sc :set sort=+ctime<cr>
nnoremap sC :set sort=-ctime<cr>
nnoremap sm :set sort=+mtime<cr>
nnoremap sM :set sort=-mtime<cr>

" ============ Visual mode ============ "

map * ggVG<cr>

vnoremap J 4j
vnoremap K 4k

nnoremap v av
vnoremap v <CR>
nnoremap rv av<c-g>
nnoremap iv av<c-g><c-g>
vnoremap w :view<cr>gv

vnoremap ,p :!sxiv -t %f & <cr>

" ------------------------------------------------------------------------------

" Pdf
filextype {*.pdf},<application/pdf> zathura %c %i &,
fileviewer {*.pdf},<application/pdf> pdftotext -nopgbrk %c -

"" md
"filextype {*.md},<text/plain> 
"        \ {Open with nvim}
"        \ vimcmd &,
"        \ {Open with typora}
"        \ typora %c %i &,
"fileviewer {*.md},<text/plain> pdftotext -nopgbrk %c -

" epub 
filextype {*.epub},<application/epub> zathura %c %i &,
fileviewer {*.epub},<application/epub> pdftotext -nopgbrk %c -

" PostScript
filextype {*.ps,*.eps,*.ps.gz},<application/postscript>
        \ {View in zathura}
        \ zathura %f,
        \ {View in gv}
        \ gv %c %i &,

" Djvu
filextype {*.djvu},<image/vnd.djvu>
        \ {View in zathura}
        \ zathura %f,
        \ {View in apvlv}
        \ apvlv %f,

" Audio
filetype {*.wav,*.mp3,*.flac,*.m4a,*.wma,*.ape,*.ac3,*.og[agx],*.spx,*.opus},
        \<audio/*>
       \ {Play using mpv 1.6 speed}
        \ mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=380x380 --geometry=-15-60 %c %i &,
       \ {Play using mpv}
        \ mpv --ontop --no-border --no-terminal --force-window --autofit=380x380 --geometry=-15-60 %c %i &,
fileviewer {*.mp3},<audio/mpeg> mediainfo
fileviewer {*.flac},<audio/flac> soxi

" Video
filextype {*.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
          \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
          \*.as[fx]},
         \<video/*>
        \ {View using mpv}
        \ mpv --speed=1.6 --ontop --no-border --no-terminal --force-window --autofit=950x380 --geometry=-15-60 %c %i &,
        \ {View using Dragon}
        \ dragon %f:p,
        \ {View using mplayer}
        \ mplayer %f,
fileviewer {*.avi,*.mp4,*.wmv,*.dat,*.3gp,*.ogv,*.mkv,*.mpg,*.mpeg,*.vob,
           \*.fl[icv],*.m2v,*.mov,*.webm,*.ts,*.mts,*.m4v,*.r[am],*.qt,*.divx,
           \*.as[fx]},
          \<video/*>
         \ ffprobe -pretty %c 2>&1

" Web
filextype {*.html,*.htm},<text/html>
        \ {Open with dwb}
        \ dwb %f %i &,
        \ {Open with firefox}
        \ firefox %f &,
        \ {Open with uzbl}
        \ uzbl-browser %f %i &,
filetype {*.html,*.htm},<text/html> links, lynx

" Object
filetype {*.o},<application/x-object> nm %f | less

" Man page
filetype {*.[1-8]},<text/troff> man ./%c
fileviewer {*.[1-8]},<text/troff> man ./%c | col -b

" Images
filextype {*.bmp,*.jpg,*.jpeg,*.png,*.gif,*.xpm},<image/*>
        \ {View in sxiv}
        \ sxiv %f &,
        \ {View in gpicview}
        \ gpicview %c,
        \ {View in shotwell}
        \ shotwell,
fileviewer {*.bmp,*.jpg,*.jpeg,*.png,*.gif,*.xpm},<image/*>
         \ sxiv %f

" OpenRaster
filextype *.ora
        \ {Edit in MyPaint}
        \ mypaint %f,

" Mindmap
filextype *.vym
        \ {Open with VYM}
        \ vym %f &,

" MD5
filetype *.md5
       \ {Check MD5 hash sum}
       \ md5sum -c %f %S,

" SHA1
filetype *.sha1
       \ {Check SHA1 hash sum}
       \ sha1sum -c %f %S,

" SHA256
filetype *.sha256
       \ {Check SHA256 hash sum}
       \ sha256sum -c %f %S,

" SHA512
filetype *.sha512
       \ {Check SHA512 hash sum}
       \ sha512sum -c %f %S,

" GPG signature
filetype {*.asc},<application/pgp-signature>
       \ {Check signature}
       \ !!gpg --verify %c,

" Torrent
filetype {*.torrent},<application/x-bittorrent> ktorrent %f &
fileviewer {*.torrent},<application/x-bittorrent> dumptorrent -v %c

" FuseZipMount
filetype {*.zip,*.jar,*.war,*.ear,*.oxt,*.apkg},
        \<application/zip,application/java-archive>
       \ {Mount with fuse-zip}
       \ FUSE_MOUNT|fuse-zip %SOURCE_FILE %DESTINATION_DIR,
       \ {View contents}
       \ zip -sf %c | less,
       \ {Extract here}
       \ tar -xf %c,
fileviewer *.zip,*.jar,*.war,*.ear,*.oxt zip -sf %c

" ArchiveMount
filetype {*.tar,*.tar.bz2,*.tbz2,*.tgz,*.tar.gz,*.tar.xz,*.txz},
        \<application/x-tar>
       \ {Mount with archivemount}
       \ FUSE_MOUNT|archivemount %SOURCE_FILE %DESTINATION_DIR,
fileviewer *.tgz,*.tar.gz tar -tzf %c
fileviewer *.tar.bz2,*.tbz2 tar -tjf %c
fileviewer *.tar.txz,*.txz xz --list %c
fileviewer {*.tar},<application/x-tar> tar -tf %c

" Rar2FsMount and rar archives
filetype {*.rar},<application/x-rar>
       \ {Mount with rar2fs}
       \ xarchiver %f,
fileviewer {*.rar},<application/x-rar> xarchiver %f

" IsoMount
filetype {*.iso},<application/x-iso9660-image>
       \ {Mount with fuseiso}
       \ FUSE_MOUNT|fuseiso %SOURCE_FILE %DESTINATION_DIR,

" SshMount
filetype *.ssh
       \ {Mount with sshfs}
       \ FUSE_MOUNT2|sshfs %PARAM %DESTINATION_DIR %FOREGROUND,

" FtpMount
filetype *.ftp
       \ {Mount with curlftpfs}
       \ FUSE_MOUNT2|curlftpfs -o ftp_port=-,,disable_eprt %PARAM %DESTINATION_DIR %FOREGROUND,

" Fuse7z and 7z archives
filetype {*.7z},<application/x-7z-compressed>
       \ {Mount with fuse-7z}
       \ FUSE_MOUNT|fuse-7z %SOURCE_FILE %DESTINATION_DIR,
fileviewer {*.7z},<application/x-7z-compressed> 7z l %c

" Office files
filextype {*.odt,*.doc,*.docx,*.xls,*.xlsx,*.csv,*.odp,*.pptx,*.ppt},
         \<application/vnd.openxmlformats-officedocument.*,
          \application/msword,
          \application/vnd.ms-excel>
        \ libreoffice %f &
fileviewer {*.doc},<application/msword> catdoc %c
fileviewer {*.docx},
          \<application/
           \vnd.openxmlformats-officedocument.wordprocessingml.document>
         \ docx2txt.pl %f -

" TuDu files
filetype *.tudu tudu -f %c

" Qt projects
filextype *.pro qtcreator %f &

" Directories
filextype */
        \ {View in thunar}
        \ Thunar %f &,


" Open selection in sxiv
filextype *.jpg,*.jpeg,*.png,*.gif
       \ {View in sxiv thumbnail mode}
       \ sxiv -t &,

" ============ Icons ============ "

" file types
set classify=' :dir:/, :exe:, :reg:, :link:'
" various file names
set classify+=' ::../::, ::*.sh::, ::*.[hc]pp::, ::*.[hc]::, ::/^copying|license$/::, ::.git/,,*.git/::, ::*.epub,,*.fb2,,*.djvu::, ::*.pdf::, ::*.htm,,*.html,,**.[sx]html,,*.xml::'
" archives
set classify+=' ::*.7z,,*.ace,,*.arj,,*.bz2,,*.cpio,,*.deb,,*.dz,,*.gz,,*.jar,,*.lzh,,*.lzma,,*.rar,,*.rpm,,*.rz,,*.tar,,*.taz,,*.tb2,,*.tbz,,*.tbz2,,*.tgz,,*.tlz,,*.trz,,*.txz,,*.tz,,*.tz2,,*.xz,,*.z,,*.zip,,*.zoo::'
" images
set classify+=' ::*.bmp,,*.gif,,*.jpeg,,*.jpg,,*.ico,,*.png,,*.ppm,,*.svg,,*.svgz,,*.tga,,*.tif,,*.tiff,,*.xbm,,*.xcf,,*.xpm,,*.xspf,,*.xwd::'
" audio
set classify+=' ::*.aac,,*.anx,,*.asf,,*.au,,*.axa,,*.flac,,*.m2a,,*.m4a,,*.mid,,*.midi,,*.mp3,,*.mpc,,*.oga,,*.ogg,,*.ogx,,*.ra,,*.ram,,*.rm,,*.spx,,*.wav,,*.wma,,*.ac3::'
" media
set classify+=' ::*.avi,,*.ts,,*.axv,,*.divx,,*.m2v,,*.m4p,,*.m4v,,.mka,,*.mkv,,*.mov,,*.mp4,,*.flv,,*.mp4v,,*.mpeg,,*.mpg,,*.nuv,,*.ogv,,*.pbm,,*.pgm,,*.qt,,*.vob,,*.wmv,,*.xvid::'
" office files
set classify+=' ::*.doc,,*.docx,,*.odt::, ::*.xls,,*.xls[mx]::, ::*.pptx,,*.ppt::'


" Single pane when started via vim plugin
if $MYVIMRC != '' | only | endif
" }}}
"


" Tabs
" ------------------------------------------------------------------------------

command! tabfmt :execute 'tabname ' . expand('%%d:' . expand('%a'))

autocmd DirEnter ** :tabfmt ~
nnoremap <c-t> :tabnew | tabfmt ~<cr>


