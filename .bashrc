# If not running interactively, don't do anything
[[ $- != *i* ]] && return

HISTSIZE= HISTFILESIZE= # Infinite history

#PS1='[\u@\h \W]\$ '
PS1="┌── \u@\h[\e[1;31m\w\e[m]\n└─╼ \$ "
PS2='> '

export PATH=/home/wn/.local/bin:$PATH
export PATH=/home/wn/.local/bin/statusbar:$PATH

alias xd='startx "$XDG_CONFIG_HOME/x11/xinitrc" dwm'
alias xb='startx "$XDG_CONFIG_HOME/x11/xinitrc" bspwm'
alias xx='startx "$XDG_CONFIG_HOME/x11/xinitrc" xmonad'


#find $1 -name '*.$2' -print0 |   wc -w --files0-from=-


# Search in bash history through fzf
alias ss="history | cut -c 8- | sort | uniq | fzf | tr -d '\n' | xclip -selection c"

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first'  # all files and dirs
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'

# dotfiles
alias config='/usr/bin/git --git-dir=$HOME/dots/ --work-tree=$HOME'


alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'


## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

#free
alias free="free -m"

#grub update
alias update-grub="sudo grub-mkconfig -o /boot/grub/grub.cfg"

#get fastest mirrors in your neighborhood
alias mirror="sudo reflector --latest 200 --sort rate --save /etc/pacman.d/mirrorlist"

# autocd (only type the directory name)
shopt -s autocd

# mpv
alias mpvaud="mpv --speed=1.6 --no-vid --"
alias mpvsv="mpv --save-position-on-quit --speed=1.6 --ontop --no-border --force-window --autofit=450x200 --geometry=-15-60"

#get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

### neovim as manpager
export MANPAGER="nvim -c 'set ft=man' -"


# youtube-dl
alias ytchannel='youtube-dl --download-archive downloaded.txt -o "%(upload_date)s - %(title)s.%(ext)s"'
alias audytchannel='youtube-dl --extract-audio --audio-format mp3 --download-archive downloaded.txt -o "%(upload_date)s - %(title)s.%(ext)s"'









alias bc='bc -q -l'					# dont show opening msg, use math library
alias cal='cal -3 | grep -B6 -A6 --color -e " $(date +%e)" -e "^$(date +%e)"' # show calendar for 3 months

alias df='df -hT --total | head -n 1 && df -hT --total | sed '1d' | sort'	# human readable, print filetype, and total
alias pdfgrep='pdfgrep -in'				# ignorecase, page number
alias wget='wget -N -c'					# continues/resumes



#-------- Youtube-dl (Stream|Download|RipAudio) {{{
#------------------------------------------------------
# DEMO: https://www.youtube.com/watch?v=MFxlwVhwayg
# DESC: download/stream media from many online sites
youtube-dl-stream() { youtube-dl -o - "$1" | $PLAYER - ;}


youtube-dl-best() { youtube-dl --restrict-filenames -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio" -o "%(title)s.%(ext)s" "$@" ;}
alias youtube-dl-thumbnail='youtube-dl --write-thumbnail --skip-download '
alias youtube-dl-480='youtube-dl -f "bestvideo[height<=480][ext=mp4]+bestaudio[ext=m4a]" '
alias youtube-dl-720='youtube-dl -f "bestvideo[height<=720][ext=mp4]+bestaudio[ext=m4a]" '
alias youtube-dl-4k='echo -e "This will transcode the video from webm to h264 which could take a long time\n\n"; youtube-dl -f "bestvideo[ext=webm]+bestaudio[ext=m4a]" --recode-video mp4 '
# alias youtube-dl-mp3='youtube-dl --extract-audio -f bestaudio[ext=mp3] --no-playlist '
alias youtube-dl-mp3='youtube-dl --ignore-errors --format bestaudio --extract-audio --audio-format mp3 --audio-quality 160K --output "%(title)s.%(ext)s"'


# DESC: convert youtube to audio (youtube.com only)
# -f 171 = webm audio (vorbis)
# -f 140 = m4a (aac)
yt2ogg() { youtube-dl --restrict-filenames -x --audio-format vorbis -o "%(title)s.%(ext)s" "$@" ;}
yt2wav() { youtube-dl --restrict-filenames -x --audio-format wav -o "%(title)s.%(ext)s" "$@" ;}
yt2mp3() { youtube-dl --restrict-filenames -x --audio-format mp3 -o "%(title)s.%(ext)s" "$@" ;}
yt2webmaudio() { youtube-dl --restrict-filenames -o "%(title)s.%(ext)s" -f 171 "$@" ;}
yt2m4a() { youtube-dl --restrict-filenames -o "%(title)s.%(ext)s" -f 140 "$@" ;}
yt2webm() { youtube-dl --restrict-filenames -o '%(title)s.%(ext)s' -f 248+251 "$@" ;}
yt2webmhighdesc() {
  GET_FORMAT=$(youtube-dl -F "$@" )
  VIDEO_ID=$(echo "$GET_FORMAT" | awk '/webm/ && /video only/ {print $1}' | tail -1)
  AUDIO_ID=$(echo "$GET_FORMAT" | awk '/webm/ && /audio only/ {print $1}' | tail -1)

  # youtube-dl -c --restrict-filenames -o '%(title)s.%(ext)s' -f 248+251 "$@"
  youtube-dl -c --restrict-filenames -o '%(title)s.%(ext)s' -f "$VIDEO_ID"+"$AUDIO_ID" "$@"
  TITLE=$(youtube-dl --get-title "$@")
  DURATION=$(youtube-dl --get-duration "$@")
  DESC=$(youtube-dl --get-description "$@")
  echo "$TITLE" >> "$TITLE".txt
  echo "$1" >> "$TITLE".txt
  echo "$DURATION" >> "$TITLE".txt
  echo "$DESC" >> "$TITLE".txt

}
yt2mp4highdesc() {
  GET_FORMAT=$(youtube-dl -F "$@" )
  VIDEO_ID=$(echo "$GET_FORMAT" | awk '/mp4/ && /video only/ {print $1}' | tail -1)
  AUDIO_ID=$(echo "$GET_FORMAT" | awk '/m4a/ && /audio only/ {print $1}' | tail -1)

  # youtube-dl -c --restrict-filenames -o '%(title)s.%(ext)s' -f 137+140 "$@"
  youtube-dl -c --restrict-filenames -o '%(title)s.%(ext)s' -f "$VIDEO_ID"+"$AUDIO_ID" "$@"
  TITLE=$(youtube-dl --get-title "$@")
  DURATION=$(youtube-dl --get-duration "$@")
  DESC=$(youtube-dl --get-description "$@")
  echo "$TITLE" >> "$TITLE".txt
  echo "$1" >> "$TITLE".txt
  echo "$DURATION" >> "$TITLE".txt
  echo "$DESC" >> "$TITLE".txt

}

# https://askubuntu.com/a/965815
yt2allwebm() {
  youtube-dl -i --all-subs --embed-subs --embed-thumbnail --add-metadata --merge-output-format webm --format 'bestvideo[ext=webm]+bestaudio[ext=webm]' "$@"
  TITLE=$(youtube-dl --get-title "$1")
  DESC=$(youtube-dl --get-duration --get-description "$1")
    echo "$TITLE\n$1\n\n$DESC" >> "$TITLE".txt
}
ytbackup() {
  # cat urllist.txt | while read -r line; do echo "$line"
  cat urllist.txt | while read -r line; do
  youtube-dl -i --all-subs --embed-subs --embed-thumbnail --add-metadata --merge-output-format mp4 --format 'bestvideo[ext=mp4]+bestaudio[ext=m4a]' "$line"
  TITLE=$(youtube-dl --get-title "$line")
  DESC=$(youtube-dl --get-duration --get-description "$line")
    echo "$TITLE\n$1\n\n$DESC" >> "$TITLE".txt
  done
}
yt2allmp4() {
  youtube-dl -i --all-subs --embed-subs --embed-thumbnail --add-metadata --merge-output-format mp4 --format 'bestvideo[ext=mp4]+bestaudio[ext=m4a]' "$@"
  TITLE=$(youtube-dl --get-title "$1")
  DESC=$(youtube-dl --get-duration --get-description "$1")
    echo "$TITLE\n$1\n\n$DESC" >> "$TITLE".txt
}






# DEMO: http://www.youtube.com/watch?v=TyDX50_dC0M
# DESC: merge multiple ip blocklist into one
# LINK: https://github.com/gotbletu/shownotes/blob/master/blocklist.sh
tsm-blocklist() {
  echo -e "${Red}>>>Stopping Transmission Daemon ${Color_Off}"
    killall transmission-daemon
  echo -e "${Yellow}>>>Updating Blocklist ${Color_Off}"
    ~/.scripts/blocklist.sh
  echo -e "${Red}>>>Restarting Transmission Daemon ${Color_Off}"
    transmission-daemon
    sleep 3
  echo -e "${Green}>>>Numbers of IP Now Blocked ${Color_Off}"
    tsm-count
}
tsm-altdownloadspeed() { transmission-remote --downlimit "${@:-900}" ;}	# download default to 900K, else enter your own
tsm-altdownloadspeedunlimited() { transmission-remote --no-downlimit ;}
tsm-limitupload() { transmission-remote --uplimit "${@:-10}" ;}	# upload default to 10kpbs, else enter your own
tsm-limituploadunlimited() { transmission-remote --no-uplimit ;}
tsm-askmorepeers() { transmission-remote -t"$1" --reannounce ;}
tsm-daemon() { transmission-daemon ;}
tsm-quit() { killall transmission-daemon ;}
tsm-add() { transmission-remote --add "$1" ;}
tsm-hash() { transmission-remote --add "magnet:?xt=urn:btih:$1" ;}       # adding via hash info
tsm-verify() { transmission-remote --verify "$1" ;}
tsm-pause() { transmission-remote -t"$1" --stop ;}		# <id> or all
tsm-start() { transmission-remote -t"$1" --start ;}		# <id> or all
tsm-purge() { transmission-remote -t"$1" --remove-and-delete ;} # delete data also
tsm-remove() { transmission-remote -t"$1" --remove ;}		# leaves data alone
tsm-info() { transmission-remote -t"$1" --info ;}
tsm-speed() { while true;do clear; transmission-remote -t"$1" -i | grep Speed;sleep 1;done ;}
tsm-grep() { transmission-remote --list | grep -i "$1" ;}
tsm() { transmission-remote --list ;}
tsm-show() { transmission-show "$1" ;}                          # show .torrent file information

# DEMO: http://www.youtube.com/watch?v=hLz7ditUwY8
# LINK: https://github.com/fagga/transmission-remote-cli
# DESC: ncurses frontend to transmission-daemon
tsm-ncurse() { transmission-remote-cli ;}





convert-pdf-to-png() {
    if [ $# -lt 1 ]
    then
        echo -e "Convert PDF document to PNG image (default dpi is 150)"
        echo -e "\nUsage:\n$0 <pdf> <dpi> <startingpage> <endingpage>"
        echo -e "\nall pages to images:\n$0 file.pdf"
        echo -e "\nchange dpi (common dpi are 150, 300, 600):\n$0 file.pdf 300"
        echo -e "\nsingle page to image:\n$0 file.pdf 300 5"
        echo -e "\nmultiple pages to images:\n$0 file.pdf 300 5 12"

        return 1
    fi
    pdftoppm -png "$1" "${1%.*}"-"${2:-150}"DPI -r "${2:-150}" -f "$3" -l "${4:-$3}"
}

convert-pdf-to-jpg() {
    if [ $# -lt 1 ]
    then
        echo -e "Convert PDF document to JPEG image (default dpi is 150)"
        echo -e "\nUsage:\n$0 <pdf> <dpi> <startingpage> <endingpage>"
        echo -e "\nall pages to images:\n$0 file.pdf"
        echo -e "\nchange dpi (common dpi are 150, 300, 600):\n$0 file.pdf 300"
        echo -e "\nsingle page to image:\n$0 file.pdf 300 5"
        echo -e "\nmultiple pages to images:\n$0 file.pdf 300 5 12"

        return 1
    fi
    pdftoppm -jpeg "$1" "${1%.*}"-"${2:-150}"DPI -r "${2:-150}" -f "$3" -l "${4:-$3}"
}













acc-beinsports-fr() { acestream-launcher --player "$ACE_PLAYER" acestream://b5950a56db8f722876dc74443d74b565fb99368f ;}













## Functions 
fzf-locate() { xdg-open "$(locate "*" | fzf -e)" ;}



fh() {
eval $(history | cut -c 8- | sort | uniq | fzf +s | sed 's/ *[0-9]* *//')
}

bind '"\C-F":"fh\n"'	# fzf history

cl() {
	local dir="$1"
	local dir="${dir:=$HOME}"
	if [[ -d "$dir" ]]; then
		cd "$dir" >/dev/null; ls
	else
		echo "bash: cl: $dir: Directory not found"
	fi
}





# These common commands are just too long! Abbreviate them.
alias \
	ka="killall" \
	g="git" \
	trem="transmission-remote" \
	YT="youtube-viewer" \
	sdn="sudo shutdown -h now" \
	e="$EDITOR" \
	v="$EDITOR" \
	p="sudo pacman" \
	xi="sudo xbps-install" \
	xr="sudo xbps-remove -R" \
	xq="xbps-query" \
	z="zathura"
