#!/bin/bash
#  ____ _____
# |  _ \_   _|  Derek Taylor (DistroTube)
# | | | || |    http://www.youtube.com/c/DistroTube
# | |_| || |    http://www.gitlab.com/dwt1/
# |____/ |_|
#
# Dmenu script for editing some of my more frequently edited config files.


declare options=("alacritty
awesome
bashrc
bspwm
neovim
polybar
sxhkd
termite
quit")

choice=$(echo -e "${options[@]}" | rofi -dmenu -i -p 'Edit config file: ')

case "$choice" in
	quit)
		echo "Program terminated." && exit 1
	;;
	alacritty)
		choice="$HOME/.config/alacritty/alacritty.yml"
	;;
	awesome)
		choice="$HOME/.config/awesome/rc.lua"
	;;
	bash)
		choice="$HOME/.bashrc"
	;;

	bspwm)
		choice="$HOME/.config/bspwm/bspwmrc"
	;;

	neovim)
		choice="$HOME/.config/nvim/init.vim"
	;;

	polybar)
		choice="$HOME/.config/polybar/config"

	;;
	sxhkd)
		choice="$HOME/.config/sxhkd/sxhkdrc"

	;;
	termite)
		choice="$HOME/.config/termite/config"
	;;

	*)
		exit 1
	;;
esac
gedit "$choice"
#alacritty -e nvim "$choice"
# emacsclient -c -a emacs "$choice"
