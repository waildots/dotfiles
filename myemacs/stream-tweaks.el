;; This file has configuration tweaks that are only meant to be used in Emacs
;; From Scratch streams.

;; Use this folder as the Emacs configuraton folder
(setq user-emacs-directory "~/myemacs")
(customize-set-value 'custom-theme-directory user-emacs-directory)

